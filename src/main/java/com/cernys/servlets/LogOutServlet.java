package com.cernys.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/logOut")
public class LogOutServlet extends HttpServlet {
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String destination = req.getParameter("dest");
    req.getSession().setAttribute("destination", destination);
    req.getSession().removeAttribute("user");
    resp.sendRedirect(req.getContextPath() + destination);
  }
}
