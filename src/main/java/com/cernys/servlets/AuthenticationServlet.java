package com.cernys.servlets;

import com.cernys.dao.*;
import com.cernys.models.User;
import com.cernys.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/authentication")
public class AuthenticationServlet extends HomeServlet {
  private DaoUsers daoUsers;
  private DaoArticle daoArticle;
  private UserService userService;

  @Override
  public void init() throws ServletException {
    DaoFactory daoFactory = DaoFactory.getInstance();
    daoUsers = new DaoUserImplement(daoFactory);
    daoArticle = new DaoArticleImplement(daoFactory);
    userService = new UserService();
  }


  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String enteredUserName = req.getParameter("username");
    String enteredPassword = req.getParameter("password");
    String destination = req.getParameter("dest");

    User user = userService.checkPassword(enteredUserName,enteredPassword);

    if (user == null) {
      req.setAttribute("errorMessage", "Nom d'utilisateur ou mot de passe incorrect");
      req.getRequestDispatcher("/WEB-INF/jsp/pages/logIn.jsp").forward(req, resp);
      req.removeAttribute("errorMessage");
    } else {
      req.getSession().setAttribute("user", user);
      if (destination == null) {
        resp.sendRedirect(req.getContextPath() + "/createArticle");
      } else {
        resp.sendRedirect(req.getContextPath() + destination);
      }

    }
  }

}
