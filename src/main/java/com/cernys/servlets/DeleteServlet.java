package com.cernys.servlets;

import com.cernys.dao.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

@WebServlet("/deleteArticle")
public class DeleteServlet extends HttpServlet {
  private DaoUsers daoUsers;
  private DaoArticle daoArticle;

  @Override
  public void init() throws ServletException{
    DaoFactory daoFactory = DaoFactory.getInstance();
    daoUsers = new DaoUserImplement(daoFactory);
    daoArticle = new DaoArticleImplement(daoFactory);
  }
  @Override
  protected void doGet (HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    Long id = Long.parseLong(req.getParameter("id"));
    if (id != null){
      daoArticle.deleteArticle(id);
      String param = URLEncoder.encode("L'article a été supprimé", "UTF-8");
      resp.sendRedirect(req.getContextPath() + "/admin?success=" + param);
    }
    else {
      String param = URLEncoder.encode("Erreur, l'article n'a pas été supprimé", "UTF-8");
      resp.sendRedirect(req.getContextPath() + "/admin?error=" + param);
    }
  }
}
