package com.cernys.servlets;

import com.cernys.dao.*;
import com.cernys.models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

@WebServlet("/saveUser")
public class SaveUserServlet extends HttpServlet {
  private DaoUsers daoUsers;
  private DaoArticle daoArticle;

  @Override
  public void init() throws ServletException{
    DaoFactory daoFactory = DaoFactory.getInstance();
    daoUsers = new DaoUserImplement(daoFactory);
    daoArticle = new DaoArticleImplement(daoFactory);
  }
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String username = req.getParameter("username");
    req.setAttribute("username", username);
    String password = req.getParameter("password");
    User user = new User();

    user.setUserName(username);
    user.setPassword(password);

    if (daoUsers.save(user)){
      String param = URLEncoder.encode("Le compte a été créé", "UTF-8");
      resp.sendRedirect(req.getContextPath() + "/logIn?success=" + param);
    } else {
      req.setAttribute("error", "Ce nom d'utilisateur existe déjà");
      req.getRequestDispatcher("/WEB-INF/jsp/pages/registration.jsp").forward(req, resp);
      req.removeAttribute("error");
    }



  }


}
