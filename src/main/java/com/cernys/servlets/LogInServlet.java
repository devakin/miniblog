package com.cernys.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/logIn")
public class LogInServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    req.setCharacterEncoding("UTF-8");
    String destination = req.getParameter("dest");
    req.getSession().setAttribute("destination", destination);
    String success = req.getParameter("success");
    req.setAttribute("success", success);
    String error = req.getParameter("error");
    req.setAttribute("error", error);
    req.getRequestDispatcher("WEB-INF/jsp/pages/logIn.jsp").forward(req, resp);
    req.removeAttribute("success");
    req.removeAttribute("error");
  }



}
