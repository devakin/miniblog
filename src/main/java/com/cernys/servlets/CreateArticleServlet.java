package com.cernys.servlets;

import com.cernys.dao.*;
import com.cernys.models.Article;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/createArticle")
public class CreateArticleServlet extends HttpServlet {
  private DaoUsers daoUsers;
  private DaoArticle daoArticle;

  @Override
  public void init() throws ServletException {
    DaoFactory daoFactory = DaoFactory.getInstance();
    daoUsers = new DaoUserImplement(daoFactory);
    daoArticle = new DaoArticleImplement(daoFactory);
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    if (req.getParameter("id") != null) {
      int id = Integer.parseInt(req.getParameter("id"));
      Article article = daoArticle.findArticleById(id);
      req.setAttribute("article", article);
      req.getSession().setAttribute("article", article);
    }
    req.getRequestDispatcher("/WEB-INF/jsp/pages/createArticle.jsp").forward(req, resp);


  }

}
