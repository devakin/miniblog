package com.cernys.servlets;

import com.cernys.dao.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/blog")
public class BlogServlet extends HttpServlet {
  private DaoUsers daoUsers;
  private DaoArticle daoArticle;

  @Override
  public void init() throws ServletException{
    DaoFactory daoFactory = DaoFactory.getInstance();
    daoUsers = new DaoUserImplement(daoFactory);
    daoArticle = new DaoArticleImplement(daoFactory);
  }

  private void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    int ARTICLES_IN_PAGE = 3;
    int articlesCount = 0;
    int page = 1;
    // transfer to browser how many articles in page
    req.setAttribute("articlesInPage", ARTICLES_IN_PAGE);

    // transfer to browser which page is open
    if (req.getParameter("page") == null){
      page = 1;
    } else{
      page = Integer.parseInt(req.getParameter("page"));
    }
    req.setAttribute("page", page);

    // Success message after creating article
    req.setCharacterEncoding("UTF-8");
    String successMessage = req.getParameter("success");
    req.setAttribute("success", successMessage);
    String errorMessage = req.getParameter("error");
    req.setAttribute("error", errorMessage);

//    try {
    articlesCount = daoArticle.findNombreArticles();
    req.setAttribute("articlesCount", articlesCount);
    req.setAttribute("articles", daoArticle.findArticlesByNumberOfPage(page, ARTICLES_IN_PAGE));
//    }
//    catch (SQLException e) {
//      // faire page d'erreur
//    }


    req.getRequestDispatcher("/WEB-INF/jsp/pages/blog.jsp").forward(req, resp);
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    execute(req, resp);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    execute(req, resp);
  }


}
