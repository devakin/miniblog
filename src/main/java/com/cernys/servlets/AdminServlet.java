package com.cernys.servlets;

import com.cernys.dao.*;
import com.cernys.models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/admin")
public class AdminServlet extends HomeServlet {
  private DaoUsers daoUsers;
  private DaoArticle daoArticle;

  @Override
  public void init() throws ServletException{
    DaoFactory daoFactory = DaoFactory.getInstance();
    daoUsers = new DaoUserImplement(daoFactory);
    daoArticle = new DaoArticleImplement(daoFactory);
  }
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String success = req.getParameter("success");
    String error = req.getParameter("error");
    req.setAttribute("success", success);
    req.setAttribute("error", error);
    User user = (User) req.getSession().getAttribute("user");

    int ARTICLES_IN_PAGE = 15;
    int articlesCount = 0;
    int page = 1;

    // transfer to browser how many articles in page
    req.setAttribute("articlesInPage", ARTICLES_IN_PAGE);

    // transfer to browser which page is open
    if (req.getParameter("page") != null){
      page = Integer.parseInt(req.getParameter("page"));
    }
    req.setAttribute("page", page);

    if (user.getRole().equals("admin")){
      articlesCount = daoArticle.findNombreArticles();
      req.setAttribute("articlesCount", articlesCount);
      req.setAttribute("articles", daoArticle.findArticlesByNumberOfPage(page, ARTICLES_IN_PAGE));
    } else {
      articlesCount = daoArticle.findNombreArticlesByUserId(user.getId().intValue());
      req.setAttribute("articlesCount", articlesCount);
      req.setAttribute("articles", daoArticle.findArticlesByPageAndUser(page, ARTICLES_IN_PAGE, user.getId().intValue()));
    }



    req.getRequestDispatcher("/WEB-INF/jsp/pages/admin.jsp").forward(req, resp);
  }
}
