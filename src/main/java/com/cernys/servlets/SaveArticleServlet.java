package com.cernys.servlets;

import com.cernys.dao.*;
import com.cernys.models.Article;
import com.cernys.models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

@WebServlet("/saveArticle")
public class SaveArticleServlet extends HttpServlet {
  private DaoUsers daoUsers;
  private DaoArticle daoArticle;

  @Override
  public void init() throws ServletException {
    DaoFactory daoFactory = DaoFactory.getInstance();
    daoUsers = new DaoUserImplement(daoFactory);
    daoArticle = new DaoArticleImplement(daoFactory);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    // to save special symbols to db
    req.setCharacterEncoding("UTF-8");

    // article creation
    String title = req.getParameter("title");
    String body = req.getParameter("body");
    User user = (User) req.getSession().getAttribute("user");
    Article article = new Article();
    // create or modify article
    Article modifiedArticle = (Article) req.getSession().getAttribute("article");
    if (modifiedArticle != null) {
      req.getSession().removeAttribute("article");
      modifiedArticle.setTitle(title);
      modifiedArticle.setBody(body);
    } else {
      article.setAuthor(user);
      article.setTitle(title);
      article.setBody(body);
    }

    boolean success;
    String redirectPage;

    if (modifiedArticle != null) {
      success = daoArticle.updateArticle(modifiedArticle);
      redirectPage = "/admin";
    } else {
      success = daoArticle.createArticle(article);
      redirectPage = "/blog";
    }

    if (success) {
      String param = URLEncoder.encode("L'article a été sauvgardé", "UTF-8");
      resp.sendRedirect(req.getContextPath() + redirectPage + "?success=" + param);
    } else {
      String param = URLEncoder.encode("Échec de la sauvgarde", "UTF-8");
      resp.sendRedirect(req.getContextPath() + redirectPage + "?error=" + param);
    }


  }


}
