package com.cernys.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;





public @Data @AllArgsConstructor @NoArgsConstructor @ToString class Article {

  private Long id;
  private User author;
  private String date;
  private String title;
  private String body;

}
