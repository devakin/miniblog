package com.cernys.dao;


import com.cernys.models.Article;
import java.util.List;

public interface DaoArticle {


  List<Article> findArticlesByNumberOfPage (int page, int nbArticle);
  List<Article> findArticlesByPageAndUser (int page, int nbArticle, int userId);
  int findNombreArticles();
  int findNombreArticlesByUserId(int userId);
  Article findArticleById (int id);
  List<Article> findAllArticles ();
  boolean createArticle(Article article);
  boolean updateArticle(Article article);
  boolean deleteArticle(Long id);


}
