package com.cernys.dao;
import com.cernys.models.User;
import org.mindrot.jbcrypt.BCrypt;

import java.sql.*;
public class DaoUserImplement implements DaoUsers {
  private DaoFactory daoFactory;
  public DaoUserImplement(DaoFactory daoFactory) {
    this.daoFactory = daoFactory;
  }
  @Override
  public User findById(long id) {
    User user = new User();

    try (Connection connection = daoFactory.getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement
           ("SELECT ID_USER, USERNAME, USER_PASSWORD, USER_ROLE FROM USERS WHERE ID_USER =" + id)) {

      ResultSet resultSet = preparedStatement.executeQuery();
      resultSet.next();
      user.setUserName(resultSet.getString("USERNAME"));
      user.setPassword(resultSet.getString("USER_PASSWORD"));
      user.setRole(resultSet.getString("USER_ROLE"));


    } catch (SQLException sqle) {
      sqle.printStackTrace();
      return null;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
    return user;
  }


  @Override
  public User findByUserName(String userName) {
    User user = new User();

    try (Connection connection = daoFactory.getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement
           ("SELECT ID_USER, USERNAME, USER_PASSWORD, USER_ROLE FROM USERS WHERE USERNAME =" + "'"+userName+"'")) {

      ResultSet resultSet = preparedStatement.executeQuery();
      resultSet.next();
      user.setId((long)resultSet.getInt("ID_USER"));
      user.setUserName(resultSet.getString("USERNAME"));
      user.setPassword(resultSet.getString("USER_PASSWORD"));
      user.setRole(resultSet.getString("USER_ROLE"));


    } catch (SQLException sqle) {
      sqle.printStackTrace();
      return null;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
    return user;
  }



  @Override
  public boolean save(User user) {
    String password = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt());

    try (Connection connection = daoFactory.getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement
           ("INSERT INTO USERS ( USERNAME , USER_PASSWORD, USER_ROLE) VALUES(?,?,?)");) {

      preparedStatement.setString(1, user.getUserName());
      preparedStatement.setString(2, password);
      preparedStatement.setString(3,"user");
      int row = preparedStatement.executeUpdate();


    } catch (SQLException sqle) {
      sqle.printStackTrace();
      return false;
    } catch (Exception e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }


}
