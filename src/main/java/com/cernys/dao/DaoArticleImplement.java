package com.cernys.dao;

import com.cernys.models.Article;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DaoArticleImplement implements DaoArticle {
  private DaoFactory daoFactory;
  private DaoUsers daoUsers;


  public DaoArticleImplement(DaoFactory daoFactory) {
    this.daoFactory = daoFactory;
  }

  @Override
  public List<Article> findArticlesByNumberOfPage(int page, int nbArticle) {
    int articleRef = (page - 1) * nbArticle;

    daoUsers = new DaoUserImplement(daoFactory);
    List<Article> listeArticles = new ArrayList<>();

    try (Connection connection = daoFactory.getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement
           ("SELECT * FROM ARTICLES ORDER BY DATE_ARTICLE DESC LIMIT " + articleRef + "," + nbArticle)) {

      ResultSet resultSet = preparedStatement.executeQuery();
      while (resultSet.next()) {
        Article article = setArticleAttributs(resultSet);
        listeArticles.add(article);
      }

    } catch (SQLException sqle) {
      sqle.printStackTrace();
      return null;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }

    return listeArticles;
  }

  @Override
  public List<Article> findArticlesByPageAndUser(int page, int nbArticle, int userId) {
    int articleRef = (page - 1) * nbArticle;

    daoUsers = new DaoUserImplement(daoFactory);
    List<Article> listeArticles = new ArrayList<>();

    try (Connection connection = daoFactory.getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement
           ("SELECT * FROM ARTICLES WHERE ID_USER = ? ORDER BY DATE_ARTICLE DESC LIMIT ?, ?")) {
      preparedStatement.setInt(1, userId);
      preparedStatement.setInt(2, articleRef);
      preparedStatement.setInt(3, nbArticle);

      try (ResultSet resultSet = preparedStatement.executeQuery();) {
        while (resultSet.next()) {
          Article article = setArticleAttributs(resultSet);
          listeArticles.add(article);
        }
      }
    } catch (SQLException sqle) {
      sqle.printStackTrace();
      return null;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }

    return listeArticles;
  }


  @Override
  public int findNombreArticles() {

    int retoutNombreArticles = 0;

    try (Connection connection = daoFactory.getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement
           ("SELECT COUNT(*) FROM ARTICLES");) {

      ResultSet resultSet = preparedStatement.executeQuery();
      if (resultSet.next()) {

        retoutNombreArticles = resultSet.getInt("COUNT(*)");

      }


    } catch (SQLException sqle) {
      sqle.printStackTrace();
      return 0;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }

    return retoutNombreArticles;
  }

  @Override
  public int findNombreArticlesByUserId(int userId) {
    int articles = 0;

    try (Connection connection = daoFactory.getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement
           ("SELECT COUNT(*) FROM ARTICLES WHERE ID_USER = ?");) {

      preparedStatement.setInt(1, userId);
      try (ResultSet resultSet = preparedStatement.executeQuery();) {
        if (resultSet.next()) {
          articles = resultSet.getInt("COUNT(*)");
        }
      }
    } catch (SQLException sqle) {
      sqle.printStackTrace();
      return 0;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
    return articles;
  }

  @Override
  public List<Article> findAllArticles() {

    daoUsers = new DaoUserImplement(daoFactory);
    List<Article> listeArticles = new ArrayList<>();

    try (Connection connection = daoFactory.getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement
           ("SELECT * FROM ARTICLES ORDER BY DATE_ARTICLE DESC")) {

      ResultSet resultSet = preparedStatement.executeQuery();
      while (resultSet.next()) {
        Article article = setArticleAttributs(resultSet);
        listeArticles.add(article);
      }

    } catch (SQLException sqle) {
      sqle.printStackTrace();
      return null;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }

    return listeArticles;
  }

  @Override
  public boolean createArticle(Article article) {
    DateFormatSymbols dfsFR = new DateFormatSymbols(Locale.FRANCE);
    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yy HH:mm:ss", dfsFR);
    Date aujourdhui = new Date();

    try (Connection connection = daoFactory.getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement
           ("INSERT INTO ARTICLES " +
             "(ID_USER, DATE_ARTICLE, TITLE_ARTICLE, BODY_ARTICLE) VALUES(?,STR_TO_DATE(?, '%d/%m/%Y %H:%i:%s'),?,?)");) {

      if (article != null) {
        preparedStatement.setInt(1, (article.getAuthor().getId().intValue()));
        preparedStatement.setString(2, formater.format(aujourdhui));
        preparedStatement.setString(3, article.getTitle());
        preparedStatement.setString(4, article.getBody());
      } else {
        System.err.println("Veuillez saisir un Article");
        throw new RuntimeException("Veuillez saisir un Article");
      }
      int row = preparedStatement.executeUpdate();

    } catch (SQLException sqle) {
      System.err.println(sqle);
      return false;
    } catch (Exception e) {
      System.err.println(e);
      e.printStackTrace();
      return false;
    }

    return true;
  }

  @Override
  public boolean updateArticle(Article article) {

    try (Connection connection = daoFactory.getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement
           ("UPDATE ARTICLES SET TITLE_ARTICLE = ? , BODY_ARTICLE = ? WHERE ID_ARTICLE = ?");) {

      if (article != null) {
        preparedStatement.setString(1, article.getTitle());
        preparedStatement.setString(2, article.getBody());
        preparedStatement.setInt(3, article.getId().intValue());
      } else {
        System.err.println("Article et null");
        throw new RuntimeException("Veuillez saisir un Article");
      }
      int row = preparedStatement.executeUpdate();

    } catch (SQLException sqle) {
      System.err.println(sqle);
      return false;
    } catch (Exception e) {
      System.err.println(e);
      e.printStackTrace();
      return false;
    }

    return true;

  }

  @Override
  public boolean deleteArticle(Long id) {
    try (Connection connection = daoFactory.getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement
           ("DELETE FROM ARTICLES WHERE ID_ARTICLE = ? ");) {

      if (id != null) {
        preparedStatement.setInt(1, id.intValue());
      } else {
        System.err.println("Article et null");
        throw new RuntimeException("Veuillez saisir un Article");
      }
      int row = preparedStatement.executeUpdate();

    } catch (SQLException sqle) {
      System.err.println(sqle);
      return false;
    } catch (Exception e) {
      System.err.println(e);
      e.printStackTrace();
      return false;
    }

    return true;
  }

  @Override
  public Article findArticleById(int id) {
    daoUsers = new DaoUserImplement(daoFactory);
    Article article = new Article();

    try (Connection connection = daoFactory.getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement
           ("SELECT * FROM ARTICLES WHERE ID_ARTICLE = ?")) {
      preparedStatement.setInt(1, id);

      try (ResultSet resultSet = preparedStatement.executeQuery()) {
        while (resultSet.next()) {
          article = setArticleAttributs(resultSet);
        }
      }
    } catch (SQLException sqle) {
      sqle.printStackTrace();
      return null;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
    return article;
  }

  private Article setArticleAttributs(ResultSet resultSet) throws SQLException {
    Article article = new Article();
    article.setId((long) resultSet.getInt("ID_ARTICLE"));
    article.setAuthor(daoUsers.findById((long) resultSet.getInt("ID_USER")));
    article.setDate(resultSet.getDate("DATE_ARTICLE").toString());
    article.setTitle(resultSet.getString("TITLE_ARTICLE"));
    article.setBody(resultSet.getString("BODY_ARTICLE"));
    return article;
  }

}
