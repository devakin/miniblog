package com.cernys.filters;


import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(urlPatterns = {"/createArticle", "/admin"})
public class AuthenticationFilter implements Filter {

  @Override
  public void doFilter(
    ServletRequest request,
    ServletResponse response,
    FilterChain chain
  ) throws IOException, ServletException {
    HttpServletRequest httpRequest = (HttpServletRequest)request;
    HttpServletResponse httpResponse = (HttpServletResponse)response;
    if ((httpRequest).getSession().getAttribute("user") == null){
      httpResponse.sendRedirect(httpRequest.getContextPath() + "/logIn?dest=" + httpRequest.getServletPath());
    } else {
      chain.doFilter(request, response);
    }
  }
}
