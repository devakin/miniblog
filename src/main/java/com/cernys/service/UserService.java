package com.cernys.service;

import com.cernys.dao.DaoFactory;
import com.cernys.dao.DaoUserImplement;
import com.cernys.dao.DaoUsers;
import com.cernys.models.User;
import org.mindrot.jbcrypt.BCrypt;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserService {
  DaoFactory daoFactory = DaoFactory.getInstance();
  DaoUsers daoUsers = new DaoUserImplement(daoFactory);


  public User checkPassword(String userName, String password) {
    User user = daoUsers.findByUserName(userName);

    if(user!=null){

      if (BCrypt.checkpw(password,user.getPassword())){

        return user;
      }
      else{

        return null;
      }
    }

   return null;
  }


}
