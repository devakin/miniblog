/*articles*/
INSERT INTO USERS (ID_USER, USERNAME, USER_PASSWORD, USER_ROLE) VALUES
    (1,"Nulla@tinciduntorci.co.uk","$2a$10$cTG/.WbmHfzk8.9wJZ5Ao..M1bmUwOlB0TLzdafW.lxkY75RytbtW","user"),
    (2,"vulputate.posuere.vulputate@ac.org","$2a$10$cTG/.WbmHfzk8.9wJZ5Ao..M1bmUwOlB0TLzdafW.lxkY75RytbtW","user"),
    (3,"eleifend.egestas.Sed@sodaleselit.org","$2a$10$cTG/.WbmHfzk8.9wJZ5Ao..M1bmUwOlB0TLzdafW.lxkY75RytbtW","user"),
    (4,"Cras.dolor@posuerecubilia.co.uk","$2a$10$cTG/.WbmHfzk8.9wJZ5Ao..M1bmUwOlB0TLzdafW.lxkY75RytbtW","user"),
    (5,"admin","$2a$10$UAOtToMmu4Af8dWwbdshVOGpllsSzr3IPjQBkkK09xxhoIzwS4DgW","admin");


INSERT INTO ARTICLES (ID_ARTICLE, ID_USER, DATE_ARTICLE, TITLE_ARTICLE, BODY_ARTICLE) VALUES
    (1,1,STR_TO_DATE('15/03/2020', '%d/%m/%Y'),"A la découverte de Svelte","Svelte, encore un framework front ?

Non, Svelte est une nouvelle librairie qui permet de gérer les composants front-end d’une application. 
Mais il existe son grand frère, Sapper, qui lui est un framework basé sur Svelte et qui permet de créer une application de type SPA.

A la grande différence des dernières librairies et frameworks Front-End, où la plupart du travail est directement exécuté au niveau du navigateur Web via le VirtualDOM, Svelte a décidé d’effectuer le gros du travail lors de la compilation afin de générer la logique de manipulation du DOM. Cette particularité offre deux avantages :

Des performances accrues car il y a moins de code à exécuter lors d’un changement d’état au sein d’un composant.
Le poids du code final est largement inférieur et dépend du nombre de fonctionnalités utilisées dans le code."),
    
    (2,2,STR_TO_DATE('23/04/2020', '%d/%m/%Y'),"Gérer l’impression d’une page web avec CSS","Malgré l’avènement des smartphones, encore beaucoup d’utilisateurs ont le réflexe d’imprimer les informations qu’ils trouvent sur internet, surtout concernant leurs achats.

Cependant, le rendu d’une page web n’est nativement pas fait pour s’afficher sur une feuille. Cela peut nous amener à avoir un résultat assez aléatoire : impression de 3 pages juste pour avoir une information, informations peu lisibles, impression d’éléments non-essentiels (comme le header ou le footer), etc.

En tant que développeurs, nous sommes habitués à utiliser les feuilles de style afin de gérer l’affichage du style sur les différents écrans. Mais il est aussi possible d’avoir un style dédié à l’impression, ce qui nous permet d’avoir un rendu différent de la version web qui sera plus qualitatif sur papier."),
    
    (3,3,STR_TO_DATE('26/04/2020', '%d/%m/%Y'),"A11y, ça vous parle ?","L’accessibilité de “tous à tout”
On entend souvent parler d’accessibilité dans les espaces publics physiques.

Par exemple : les places de parking réservées, proches d’une entrée de magasin, les rampes d’accès à la place d’escaliers, les boutons d’ascenseurs en braille, etc. Ces aménagements font parti intégrante de notre quotidien et nous n’y prêtons plus attention.

Mais qu’en est-il de l’accessibilité web ? Comment les personnes avec un handicap arrivent-elles à naviguer sur le web et accèdent-elles à l’information ?

On retrouve ainsi dans l’article 47 de la loi du 11 février 2005 pour l’égalité des droits et des chances, la participation et la citoyenneté des personnes handicapées, les obligations légales concernant l’accessibilité numérique.

Le principe de cette loi est simple : l’accessibilité de “tous à tout” car l’accès à l’information et à la communication est un droit universel au-delà des différences de chacun."),

    (4,4,STR_TO_DATE('03/05/2020', '%d/%m/%Y'),"Héberger deux applications Angular sur le même serveur Nginx","Introduction
On nous demande souvent comment héberger nos applications front-end et plus particulièrement nos applications Angular. Dans la plupart des cas c’est très simple, les webApps Angular étant des applications statiques, elles sont servies par un serveur Apache ou Nginx avec un système de redirection qui renvoie toutes les routes vers le fichier index.html.

Mais parfois nous avons besoin de servir plusieurs webApps sur le même domaine. On a alors la possibilité d’utiliser des sous-domaines pour séparer nos applications app1.mondomaine.com et app2.mondomaine.com et configurer le serveur web pour servir les deux applications en fonction du nom de domaine (configuration des VirtualHosts).

Mais dans le cas où l’on ne veut pas utiliser de sous-domaines et héberger nos deux applications sur le même domaine, la mise en place devient moins évidente. Dans cet article, nous allons étudier la mise en place d’un serveur web répondant à un seul nom de domaine et qui servira deux applications web. Nous allons pour cela créer deux applications Angular, et les héberger sur le même serveur Nginx l’une à l’adresse http://www.mondomaine.com/ et la seconde sur http://www.mondomaine.com/appTwo/"),

    (5,5,STR_TO_DATE('11/05/2020', '%d/%m/%Y'),"Un point sur les sprites CSS","Selon httparchive.org, le poids des images correspond à environ 48% du poids total de la page.

Il existe plusieurs méthodes pour optimiser le chargement des images d’un site web :

Comme nous avons pu le voir dans un précédent article, l’utilisation des SVG est fortement recommandée afin d’alléger au maximum le poids d’une page.

Une autre méthode d’optimisation consiste à réunir toutes les images décoratives en une seule grande image : c’est ce qu’on appelle un sprite. L’utilisation des SVG n’est pas incompatible avec l’utilisation des sprites. Les deux peuvent être complémentaires selon les cas d’utilisation.

Découvrez les avantages et les contextes dans lesquels l’utilisation des sprites CSS peuvent être utiles."),

    (6,1,STR_TO_DATE('17/05/2020', '%d/%m/%Y'),"L’utilisation des images SVG dans un projet web","Quand il s’agit d’utiliser des pictogrammes ou tout simplement afficher une image décorative, nous avons à notre disposition différentes méthodes.

Celle qui parait instinctivement la plus simple consiste à appeler des images en PNG dans un dossier dédié. Cependant, non seulement le rendu n’est pas optimal, mais cela reste une méthode peu optimisée en termes de chargement et de maintenabilité du projet.

C’est pourquoi nous allons aborder le sujet des images SVG (Scalable Vector Graphics : un format d’image de plus en plus utilisé qui permet de pallier ces problématiques."),

    (7,2,STR_TO_DATE('20/05/2020', '%d/%m/%Y'),"DotJS expérience","Les 5 et 6 décembre 2019, la dernière édition de la DotJS se tenait au Dock Pullman à Aubervilliers. Une conférence dédiée au web et plus particulièrement à son langage de prédilection, le JavaScript.

Grande nouveauté cette année, la conférence se déroule sur 2 jours avec une première journée dédiée au développement front avec que du JavaScript au menu (avec ou sans framework) et une seconde journée dédiée au langage et au JavaScript côté back (NodeJS)."),

    (8,3,STR_TO_DATE('28/05/2020', '%d/%m/%Y'),"Créer et tester vos composants React avec Jest enzyme","React par-ci, React par-là… vous en entendez parler tous les jours, mais savez-vous tester des composants à la mode de chez nous ? Beaucoup de développeurs pensent encore que les tests unitaires sont une véritable perte de temps, et pourtant, c’est un gage de qualité (pas le seul) qui nous permet de vérifier, la non-régression d’une application, tout au long de la création de features. Des librairies existent pour faciliter l’écriture de tests telles que jest-enzyme. Venez marcher dans mes pas à travers ce tutoriel qui vous mènera vers des horizons ensoleillés saupoudrés de tests.

Présentation du projet
Dans ce tutoriel, nous allons créer un formulaire composé de trois étapes permettant de visualiser une espèce de chat ou de chien grâce aux API thedogapi et thecatapi. Nous partirons d’une branche contenant une première version statique. Chaque composant sera testé unitairement avec la librairie jest-enzyme.

Les étapes du formulaire sont les suivantes :

Choix de l’animal, chat ou chien
Choix de la race parmi les races de l’animal sélectionné
Affichage d’une image et de la description de la race sélectionnée, un bouton permet de recharger une nouvelle image");


