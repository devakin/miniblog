<%@ page
  info="Display address weather and the eventual result"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%-- head --%>
<c:import url="/WEB-INF/jsp/_head.jsp"/>

<body>

<!-- Navigation -->
<c:import url="/WEB-INF/jsp/_nav.jsp"/>

<!-- Page Header -->
<header class="masthead" style="background-image: url('<c:url value="/static/images/home-bg.jpg"/>')">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="site-heading">
          <h1>A&C dev Blog</h1>
          <span class="subheading">Identification</span>
        </div>
      </div>
    </div>
  </div>
</header>

<!-- Main Content -->
<div class="container">
  <div class="row">
    <div class="col-lg-8 col-md-10 mx-auto">

      <form action="<c:url value="/authentication"/>" method="post">

        <div class="d-flex flex-column flex-grow-1">
          <div class="row">
            <p class="text-danger">
              <c:if test="${not empty error}">
                <c:out value="${error}"/>
              </c:if>
            </p>
            <p class="text-success">
              <c:if test="${not empty success}">
                <c:out value="${success}"/>
              </c:if>
            </p>
          </div>


          <div class="form-group row flex-grow-1 pb-3 mx-3">
            <label for="username" class=" px-3">Nom d'utilisateur</label>
            <input type="text" name="username" class=" form-control flex-grow-1"
                   id="username" required>
          </div>

          <div class="form-group row flex-grow-1 pb-3 mx-3">
            <label for="password" class="px-3">Mot de passe</label>
            <input type="password" class="form-control flex-grow-1"
                   id="password" name="password" required/>
          </div>

          <input type="text" name="dest" value="${destination}" hidden/>

        </div>

        <div class="clearfix">
          <div class="row justify-content-between mx-3 mx-md-0">
            <button class="btn btn-primary mt-1" type="submit">
              Se Connecter
            </button>
            <a class="btn btn-primary mt-1" type="button" href="<c:url value="/registration"/>">S'inscrire</a>
          </div>
          <small>
            <a class="form-text text-muted mx-3 mx-md-0" href="">Vous avez oublié votre mot de passe ?</a>
          </small>
        </div>
      </form>
      <!-- create -->


    </div>
  </div>
</div>

<hr>

<!-- Footer -->
<c:import url="/WEB-INF/jsp/_footer.jsp"/>


<!-- Bootstrap core JavaScript -->
<script src="<c:url value="static/scripts/template/jquery/jquery.min.js"/>"></script>
<script src="<c:url value="static/scripts/template/bootstrap/js/bootstrap.bundle.min.js"/>"></script>

<!-- Custom scripts for this template -->
<script src="<c:url value="static/scripts/clean-blog.min.js"/>"></script>


</body>

</html>
