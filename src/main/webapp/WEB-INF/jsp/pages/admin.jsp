<%@ page
  info="Display address weather and the eventual result"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%-- head --%>
<c:import url="/WEB-INF/jsp/_head.jsp"/>

<body>

<!-- Navigation -->
<c:import url="/WEB-INF/jsp/_nav.jsp"/>

<!-- Page Header -->
<header class="masthead" style="background-image: url('<c:url value="/static/images/blog-bg.jpeg"/>')">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="site-heading">
          <h1>A&C dev Blog</h1>
          <span class="subheading">Gestion des articles</span>
        </div>
      </div>
    </div>
  </div>
</header>

<!-- Main Content -->
<div class="container">
  <div class="row">
    <div class="col-lg-8 col-md-10 mx-auto">

      <%--  messages  --%>
      <c:choose>
        <c:when test="${not empty success}">
          <p class="text-success">${success}</p>
        </c:when>
        <c:when test="${not empty error}">
          <p class="text-danger">${error}</p>
        </c:when>
        <c:otherwise>
        </c:otherwise>
      </c:choose>

      <%-- add articles --%>
      <c:forEach items="${articles}" var="item">
        <div class="row flex-column">
          <div class="card">
            <div class="card-header">
              <h2 class="post-title">${item.title}</h2>
            </div>
            <div class="card-body">
              <p class="post-meta my-1">Posté par <strong>${item.author.userName}</strong> le ${item.date}
              </p class="post-meta">
            </div>
            <div class="card-footer">
              <div>
                <a href="<c:url value="/createArticle?id=${item.id}"/>">Modifier</a>
                <span>|</span>
                <a href="<c:url value="/deleteArticle?id=${item.id}"/>">Supprimer</a>
              </div>
            </div>
          </div>
        </div>
        <hr>
      </c:forEach>

    </div>
  </div>

</div>
<div class="row d-flex justify-content-center my-3">
  <ul class="pagination">
    <li id="previous" class="page-item">
      <a class="page-link custom-color" href="<c:url value="/admin?page=${page - 1}"/>">Précédent</a>
    </li>
    <%--  check how many pages --%>

    <c:choose>
      <c:when test="${articlesCount le articlesInPage}">
        <c:set var="pages" scope="page" value="1"/>
      </c:when>
      <c:when test="${articlesCount % articlesInPage == 0}">
        <c:set var="pages" scope="page" value="${articlesCount/articlesInPage}"/>
      </c:when>
      <c:otherwise>
        <fmt:parseNumber var="count" type="number" value="${articlesCount/articlesInPage}"/>
        <c:set var="pages" scope="page" value="${count + 1}"/>
      </c:otherwise>
    </c:choose>

    <%--  add pagination  --%>
    <c:forEach var="i" begin="1" end="${pageScope.pages}" step="1">
      <%--      add link property  --%>
      <li class="page-item"><a class="page-link custom-color" href="<c:url value="/admin?page=${i}"/>">${i}</a></li>
    </c:forEach>

    <li id="next" class="page-item"><a class="page-link custom-color" href="<c:url value="/admin?page=${page + 1}"/>">Suivant</a>
    </li>
  </ul>
  <span id="pageNumber" hidden><c:out value="${page}"/></span>
  <span id="totalPages" hidden><c:out value="${pageScope.pages}"/></span>
</div>
<%-- Pagination --%>


<hr>
<!-- Footer -->
<c:import url="/WEB-INF/jsp/_footer.jsp"/>


<!-- Bootstrap core JavaScript -->
<script src="<c:url value="static/scripts/template/jquery/jquery.min.js"/>"></script>
<script src="<c:url value="static/scripts/template/bootstrap/js/bootstrap.bundle.min.js"/>"></script>

<!-- Custom scripts for this template -->
<script src="<c:url value="static/scripts/clean-blog.min.js"/>"></script>
<script src="<c:url value="static/scripts/blog.js"/>"></script>

</body>

</html>
