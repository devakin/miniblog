document.addEventListener('DOMContentLoaded', () => {
  const password = document.getElementById('password')
  const passwordConfirmation = document.getElementById('passwordConfirmation')

  function checkConfirmation (e){
    const p = document.querySelector('p.text-danger')
    p.innerText = ""
    if(password.value !== passwordConfirmation.value){
      e.preventDefault()
      p.innerText = "Le mot de passe ne correspond pas"
    }
  }
  document.addEventListener('submit', (e)=>{
    checkConfirmation(e)
  })


})
